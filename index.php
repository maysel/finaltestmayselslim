<?php
require "bootstrap.php";
use Chatter\Models\Message;
use Chatter\Models\User;
use Chatter\Models\Car;
use Chatter\Models\Product;
//use Chatter\Middleware\Logging;

$app = new \Slim\App();
//$app->add(new Logging());
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at,
            'updated_at'=>$msg->updated_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

//---cars functions---
$app->get('/cars', function($request, $response,$args){
   $_car = new Car();
   $cars = $_car->all();
   $payload = [];
   foreach($cars as $msg){
        $payload[$msg->id] = [
            'carPlateNum'=>$msg->carPlateNum,
            'carBrand'=>$msg->carBrand,
            'created_at'=>$msg->created_at,
            'updated_at'=>$msg->updated_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

//---end of cars functions---




//---users functions---
$app->get('/users', function($request, $response,$args){
   $_user = new User();
   $users = $_user->all();
   $payload = [];
   foreach($users as $usr){
        $payload[$usr->id] = [
            'id'=>$usr->id,
            'username'=>$usr->username,
            'email'=>$usr->email
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/users', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');
   $_user = new User();
   $_user->username = $username;
   $_user->email = $email;
   $_user->save();
   if($_user->id){
       $payload = ['user id: '=>$_user->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/users/{id}', function($request, $response,$args){
    $user = User::find($args['id']);
    $user->delete();
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});


//added this for user-update
$app->get('/users/{id}', function($request, $response,$args){
    $_id = $args['id'];
    $message = User::find($_id);
    return $response->withStatus(200)->withJson($message);
});


$app->put('/users/{user_id}', function($request, $response,$args){
    $username = $request->getParsedBodyParam('username','');
    $email = $request->getParsedBodyParam('email','');;
    $_user = User::find($args['user_id']);
    $_user->username = $username;
    $_user->email = $email;

    if($_user->save()){ //אם הערך נשמר
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});


//---end of users functions---

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});


//LOGIN WITHOUT JWT
$app->post('/login', function($request, $response,$args){
    $username  = $request->getParsedBodyParam('username','');
    $password = $request->getParsedBodyParam('password','');    
    $_user = User::where('username', '=', $username)->where('password', '=', $password)->get();
    
    if($_user[0]->id){
        $payload = ['success'=>true];
        return $response->withStatus(201)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
    else{
        return $response->withStatus(400)->withHeader('Access-Control-Allow-Origin', '*');
    }
});


//products funcations 

$app->get('/products', function($request, $response,$args){
   $_product = new Product();
   $products = $_product->all();
   $payload = [];
   foreach($products as $prod){
        $payload[$prod->id] = [
            'id'=>$prod->id,
            'name'=>$prod->name,
            'price'=>$prod->price,
            //'created_at'=>$prod->created_at,
            //'updated_at'=>$prod->updated_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});


$app->get('/products/{id}', function($request, $response,$args){
$_id = $args['id'];
$product = Product::find($_id);

   return $response->withStatus(200)->withJson($product);
});


$app->post('/products', function($request, $response,$args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
   $_product = new Product();
   $_product->name = $name;
   $_product->price = $price;
   $_product->save();
   if($_product->id){
       $payload = ['product id: '=>$_product->id];
       return $response->withStatus(201)->withJson($payload);
   }
   else{
       return $response->withStatus(400);
   }
});


$app->delete('/products/{id}', function($request, $response,$args){
    $product = Product::find($args['id']);
    $product->delete();
    if($product->exists){
        return $response->withStatus(400);
    }
    else{
         return $response->withStatus(200);
    }
});

$app->put('/products/{id}', function($request, $response,$args){
    $id = $request->getParsedBodyParam('id','');
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');;
    $_product = Product::find($args['id']);
    $_product->name = $name;
    $_product->price = $price;

    if($_product->save()){ //אם הערך נשמר
        $payload = ['id' => $_product->id,"result" => "The product has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->post('/products/search', function($request, $response, $args){    
    $name = $request->getParsedBodyParam('name',''); 
    $products  = Product::where('name', '=', $name)->get(); 
    $payload=[];
    foreach($products as $prd){
        $payload[$prd->id] = [
            'id'=> $prd->id,
            'name'=> $prd->name,
            'price'=> $prd->price
        ];
    }
    return $response->withStatus(200)->withJson($payload)->withHeader('Access-Control-Allow-Origin', '*');
    }
);
 

///end of products funcations

/*
$app->get('/messages/{id}', function($request, $response,$args){
$_id = $args['id'];
$message = Message::find($_id);

   return $response->withStatus(200)->withJson($message);
});*/


$app->run();
