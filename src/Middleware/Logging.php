<?php

namespace Chatter\Middleware;

class Logging{
    public function __invoke($request,$response,$next){
        //before rout
        error_log($request->getMethod()."--".$request->getUri()."\r\n",3,"log.txt"); // כתיבה של הדטרינג לתוך קובץ 
        $response = $next ($request,$response);
        //after rout
        return $response;
    }
}